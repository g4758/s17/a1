console.log("JS ARRAYS");

/*
	Array
		-collection of multiple related data/values
*/

let month = ["Jan", "Feb", "Mar", "Apr", "May", "June"];

// console.log(month); //array
// console.log(month.length); // 5 - count elements in array 
// console.log(month[3]) //will show index no. in "month" array

//using for loop 

/*for(let i = 0; i < month.length; i++ ){

	console.log(`Month of ${month[i]}`); //displaying each element in an array "using index"
}*/

// How to initialize an array?
	let fruits = ["Apple", "banana", "strawberry"];

	let phones = new Array("iphone", "samsung", "nokia");
	console.log(phones);

/*
	Array Manipulation
*/
	let count = ["one", "two", "three", "four", "five"];
	console.log(count);

	//Push () method
		//adds an element at the end of the array
		count.push("six");
		console.log(count);

		function push(element){
			// add "seven" element in an array
			count.push(element)
		}
		push("seven");
		console.log(count);

		push("eight");
		console.log(count);

	//Pop() method
		//remove an element at the end of an array
		count.pop();
		console.log(count)

		function Pop(){
			count.pop()
		}

		Pop();
		console.log(count);

		Pop();
		console.log(count);

	//Shift() method
		//removes an element at the beginning of an array
		count.shift();
		console.log(count);

	//Unshift() method
		// adds an element at the beginning of an array
		count.unshift("one");
		console.log(count);

	let digits = [5, "one", 8, 6, 3, 9, 4, 500, 840, 239, "ten"];
	// Sort( method)

	digits.sort();
	console.log(digits);

	digits.sort(
		function(a,b){
			// ascending order
			// return a - b

			// descending order
			return b - a
		}
	);

	console.log(digits);

/*
	Slice and Splice
*/

	//Splice
		//modifies an array

		//1st param - index number
		//2nd param - # of elements to be deleted
		//3rd param - added in placement of deleted elements

		console.log(month);

	let nSplice = month.splice(1, 2, "July", "Aug", "Sept");
	console.log(month);

	console.log(nSplice);

	// Slice
		//returns a shallow copy of a portion of an array

		//1st param - index number (start) 
		// 2nd param - index number where to stop

		console.log(month);

	// let nSlice = month.slice(2);  //from index 2-last element
	// let nSlice = month.slice(2,4); //from index 2-3
	
	// To copy the elements from index 3 up to the last element, what values of params should be used?

	// Answer:
	// let nSlice = month.slice(3, month.length); 

	// console.log(month);
	// console.log(nSlice);

	// Concat() method
		// to merge two or more arrays
		// console.log(month);
		// console.log(fruits);
		// console.log(phones);

// let concatArray = month.concat(fruits,phones);
// 	console.log(month);
// 	console.log(fruits);
// 	console.log(phones);
// 		console.log(concatArray);


	// Join() method
		//

		//params:
			// " "
			// "-"
			// ","

	console.log(month);
	let newJoin = month.join(", ");
	console.log(newJoin);
	// console.log(typeof newJoin);

	// toString() method
		let elements = [1,2, "a", "3b"];

		let nToString = elements.toString()
		console.log(nToString);
		// console.log(typeof nToString);

/*
	Accessors
*/
	// let countries = ["US", "RU", "CAN", "PH", "SG", "HK", "PH"];

	// indexOf() 
		// We will find PH in the countries array
		// Param - value

	// console.log(countries.indexOf("PH")); //3
	// console.log(countries.indexOf("NZ")); //-1

	// if(countries.indexOf("NZ") !== -1){
	// 	alert("PH is in the countries array")
	// } else {
	// 	alert("Country does not exist") 
	// }

	// lastIndexOf()
		// param - last value

	// console.log(countries.lastIndexOf("PH")); //6


/*
	Iterators

		//forEach()
			//executes a provided function "once for each element"

		syntax of forEach():

		forEach(callback fn)

			syntaxof callback function:
			
			function(<parameter>){
				//statement
			}
*/

	// console.log(month);

	// month.forEach(
	// 	function(element){
	// 		console.log(element)
	// 	} 
	// );

let names = ["Angelito", "Ian", "Gerard", "Mikhaella", "Matthew"];

let mapNames = names.map(
	function(student){
		return(
			`
				<p>My name is ${student}</p>
			`
			)
	}
)

let container = document.getElementById(`display`);

container.innerHTML = mapNames;


/*
	filter()
		//creates a new array that contains elements which passed a given condition

		//does not modify 

*/

let nums = [1, 2, 3, 4, 5];

let filteredNums = nums.filter(function(red){
	//console.log(red)

	return red < 4
})

console.log(filteredNums);


/*
	//includes




function cWord(word){
	if(animals.includes(word)){
		return word
	} else {
		return `Animal does not exist`
	}
}

console.log(cWord("bird"));
console.log(cWord("Horse"));

*/

/*
	//Every()
		// The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
*/

console.log(nums)

let result = nums.every(function(element){
	return element > 0
})

console.log(result);

/*
	//Some()
		// The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
*/

let Someresult = nums.some(function(element){
	return element > 3
})

console.log(Someresult);